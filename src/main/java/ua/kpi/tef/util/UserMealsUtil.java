package ua.kpi.tef.util;

import ua.kpi.tef.model.UserMeal;
import ua.kpi.tef.model.UserMealWithExceed;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalField;
import java.util.*;
import java.util.stream.Collectors;

/**
 * GKislin
 * 31.05.2015.
 */
public class UserMealsUtil {

    public static void main(String[] args) {
        List<UserMeal> mealList = Arrays.asList(
                new UserMeal(LocalDateTime.of(2015, Month.MAY, 30,10,0), "Завтрак", 500),
                new UserMeal(LocalDateTime.of(2015, Month.MAY, 30,13,0), "Обед", 1000),
                new UserMeal(LocalDateTime.of(2015, Month.MAY, 30,20,0), "Ужин", 500),
                new UserMeal(LocalDateTime.of(2015, Month.MAY, 31,10,0), "Завтрак", 1000),
                new UserMeal(LocalDateTime.of(2015, Month.MAY, 31,13,0), "Обед", 500),
                new UserMeal(LocalDateTime.of(2015, Month.MAY, 31,20,0), "Ужин", 510)
        );
        getFilteredWithExceededUsingStream(mealList, LocalTime.of(7, 0), LocalTime.of(12,0), 2000).forEach(System.out::println);
    }


    public static List<UserMealWithExceed>  getFilteredWithExceeded(List<UserMeal> mealList, LocalTime startTime, LocalTime endTime, int caloriesPerDay) {
        Map<LocalDate, Long> map = new HashMap<>(mealList.size());
        List<UserMealWithExceed> filtered = new ArrayList<>(mealList.size());
        for (UserMeal meal: mealList) {
            map.compute(meal.getDateTime().toLocalDate(), (key, value) -> {
                if (value == null) {
                    value = 0L;
                }
                value += meal.getCalories();
                if (TimeUtil.isBetween(meal.getDateTime().toLocalTime(), startTime, endTime)) {
                    filtered.add(new UserMealWithExceed(meal));
                }
                return value;
            });
        }

        for (UserMealWithExceed entry : filtered) {
            entry.setExceed(map.get(entry.getDateTime().toLocalDate()) > caloriesPerDay);
        }

        return filtered;
    }

    public static List<UserMealWithExceed>  getFilteredWithExceededUsingStream(List<UserMeal> mealList, LocalTime startTime, LocalTime endTime, int caloriesPerDay) {
        Map<LocalDate, Long> map = new HashMap<>(mealList.size()); //read write O(1)

        List<UserMealWithExceed> filtered = mealList.stream()
                .peek(meal -> map.merge(meal.getDateTime().toLocalDate(), (long) meal.getCalories(), Long::sum))
                .filter(meal -> TimeUtil.isBetween(meal.getDateTime().toLocalTime(), startTime, endTime))
                .map(UserMealWithExceed::new)
                .collect(Collectors.toList()); //O(n)

        filtered.forEach(meal -> meal.setExceed(map.get(meal.getDateTime().toLocalDate()) > caloriesPerDay)); //O(n)

        return filtered; // = O(2n) = O(n)
    }

}
